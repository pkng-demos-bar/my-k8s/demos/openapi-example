# Using official python runtime base image
FROM python:3.9-slim

# add curl for healthcheck
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    curl \
    && rm -rf /var/lib/apt/lists/*

#RUN \
#    apt-get update \
#    && apt-get install -y \
#    build-essential
 
RUN \
    python -m pip install --upgrade pip \
    && python -m pip install wheel

ADD . /app

WORKDIR /app

RUN python -m pip install -r requirements.txt

CMD ["python", "./rest_target.py"]
